# Damien Sambo

__Postdoc researcher__

Research team: _FUN_

[My personal website](https://wsdamieno.github.io/Site_perso/#home)

# DEV

- C
- C++
- Matlab
- Git
  - Github
  - Gitlab
- Python
- Java

My favorite linux commands are `ls` and `kill-9`

# My top 3 tv shows

1. Luky luke
2. The bing ban theory
3. One Piece

# My top 3 emoticons

:sweat_smile:  -  :open_mouth:  -  :smirk: 

Code is :
```
:sweat_smile:  -  :open_mouth:  -  :smirk: 
```

